extends Camera2D

var velocity := Vector2()
var _no_keys
export var speed := 300

func _ready():
	pass

func _process(delta):
	velocity = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down") * speed
	#print(velocity)
	position += velocity * delta
	# Fix camera sticking.
	if position.x < limit_left:
		position.x = limit_left
	elif (position.x + 640) > limit_right:
		position.x = limit_right - 640
	if position.y < limit_top:
		position.y = limit_top
	elif (position.y + 480) > limit_bottom:
		position.y = limit_bottom - 480
	

func _on_mouse_entered_side(direction:Vector2):
	velocity += direction * speed

func _on_mouse_exited_side(direction:Vector2):
	velocity -= direction * speed
