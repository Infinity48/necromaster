extends GenericEntity

enum State {WANDER, RUN}

const ZOMBIE = preload("res://objects/Zombie.tscn")

export var health := 3
export var speed := 100

var nearest_zomb:Node
var velocity := Vector2()
var state:int = State.WANDER
var being_eaten := false

onready var world := get_tree().current_scene
onready var nav_agent:NavigationAgent2D = $NavigationAgent

func _ready():
	randomize()
	nav_agent.set_navigation(world.get_node("Navigation"))
	nav_agent.set_target_location(Vector2(rand_range(100, 924), rand_range(100, 500)))
	goto_random_position()

func _process(delta):
	if state != State.RUN:
		update_nearest_zombie()

func _physics_process(delta):
	if nearest_zomb:
		velocity = (position - nearest_zomb.position).normalized() * speed
		change_state(State.RUN)
	else:
		var next_pos = nav_agent.get_next_location()
		nav_agent.set_velocity(velocity)
		velocity = (next_pos - position).normalized() * speed
		if not nav_agent.is_target_reachable():
			#print("can't go")
			_on_NavTimer_timeout()
		change_state(State.WANDER)
	rotation = velocity.angle()
	velocity = move_and_slide(velocity)

func become_zombie():
	var new_zomb = ZOMBIE.instance()
	new_zomb.transform = transform
	world.add_child(new_zomb)
	if not SfxManager.is_sound_playing("137sounds_zombie-growl.wav"):
		SfxManager.play_sound_at("137sounds_zombie-growl.wav", global_position, 1.5, rand_range(0.8, 1.2))
	queue_free()

func update_nearest_zombie():
	var lowest_dist_sq = 9216.0 # 96x96 radius
	var new_dist_sq = 0.0
	nearest_zomb = null
	for zombie in get_tree().get_nodes_in_group("zombies"):
		new_dist_sq = position.distance_squared_to(zombie.position)
		if new_dist_sq < lowest_dist_sq:
			lowest_dist_sq = new_dist_sq
			nearest_zomb = zombie

func change_state(new_state:int):
	if new_state == state: 
		return
	match new_state:
		State.WANDER:
			$NavTimer.start(3.0)
		State.RUN:
			$NavTimer.start(0.5)
	state = new_state

func _on_KillArea_body_entered(body: Node):
	if body.is_in_group("zombies"):
		being_eaten = true
		$EatTimer.start(0.1)

func _on_KillArea_body_exited(body):
	if body.is_in_group("zombies"):
		being_eaten = false
		$EatTimer.stop()

func _on_NavTimer_timeout():
	nav_agent.set_target_location(Vector2(rand_range(100, 924), rand_range(100, 500)))
	update_nearest_zombie()

func _on_EatTimer_timeout():
	health -= 1
	if health <= 0:
		call_deferred("become_zombie")
	$EatTimer.start(1.0)

func _on_NavigationAgent_velocity_computed(safe_velocity):
	print(safe_velocity)
	velocity = safe_velocity
