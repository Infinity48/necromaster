extends Node

var _playing_sounds := {}

func play_sound(name:String, volume:float = 1.0, pitch:float = 1.0) -> AudioStreamPlayer:
	var new_sound = AudioStreamPlayer.new()
	new_sound.name = name
	new_sound.stream = load("res://sounds/" + name)
	new_sound.volume_db = linear2db(volume)
	new_sound.pitch_scale = pitch
	new_sound.connect("finished", self, "_on_sound_finished", [new_sound])
	add_child(new_sound)
	new_sound.play()
	
	_playing_sounds[name] = new_sound
	return new_sound

func play_sound_at(name:String, position:Vector2, volume:float = 1.0, pitch:float = 1.0) -> AudioStreamPlayer2D:
	var new_sound = AudioStreamPlayer2D.new()
	new_sound.name = name
	new_sound.stream = load("res://sounds/" + name)
	new_sound.volume_db = linear2db(volume)
	new_sound.pitch_scale = pitch
	new_sound.position = position
	new_sound.connect("finished", self, "_on_sound_finished", [new_sound])
	add_child(new_sound)
	new_sound.play()
	
	_playing_sounds[name] = new_sound
	return new_sound

func is_sound_playing(name:String) -> bool:
	if _playing_sounds.get(name):
		return true
	return false

func _on_sound_finished(snd):
	for snd_name in _playing_sounds.keys():
		var snd_info = _playing_sounds[snd_name]
		if snd_info == snd:
			_playing_sounds.erase(snd_name)
	snd.queue_free()
