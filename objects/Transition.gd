extends CanvasLayer

enum dir {NONE, IN, OUT}
var direction:int = dir.NONE
var next_scene:String = ''

onready var _tree := get_tree()
onready var _view := get_viewport()

func _process(delta):
	$Rect.rect_size = _view.size
	if direction == dir.IN:
		if $Rect.color.a <= 0: direction = dir.NONE
		$Rect.color.a -= delta * 2
	elif direction == dir.OUT:
		if $Rect.color.a >= 1:
			direction = dir.IN
			if next_scene:
				_tree.change_scene(next_scene)
				next_scene = ''
		$Rect.color.a += delta * 2

func change_scene(scene:String):
	next_scene = scene
	direction = dir.OUT
