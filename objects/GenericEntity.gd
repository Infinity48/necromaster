class_name GenericEntity
extends KinematicBody2D

var no_repos := false

onready var _repos_area:Area2D = $ReposArea

func _ready():
	randomize()

func _process(delta):
	if no_repos:
		return
	for body in _repos_area.get_overlapping_bodies():
		if body is TileMap:
			print("repos")
			goto_random_position()

func goto_random_position():
	position = Vector2(rand_range(100, 924), rand_range(100, 500))

