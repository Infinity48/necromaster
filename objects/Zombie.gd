extends GenericEntity

const GROAN_SOUND := "137sounds_zombie-growl.wav"

var velocity := Vector2()
var target := Vector2()

export var speed := 50

onready var world := get_tree().current_scene
onready var groan_timer:Timer = $GroanTimer

func _ready():
	randomize()
	#groan_timer.start(rand_range(30.0, 60.0))

func _process(delta):
	velocity = (target - position).normalized() * speed

func _input(event):
	if event is InputEventMouseMotion:
		target = get_global_mouse_position() + Vector2(rand_range(-64, 64), rand_range(-64, 64))
		look_at(get_global_mouse_position())

func _physics_process(delta):
	velocity = move_and_slide(velocity)

func _on_GroanTimer_timeout():
	print(SfxManager.is_sound_playing(GROAN_SOUND))
	if not SfxManager.is_sound_playing(GROAN_SOUND):
		SfxManager.play_sound_at(GROAN_SOUND, global_position)
	groan_timer.start(rand_range(30.0, 60.0))
