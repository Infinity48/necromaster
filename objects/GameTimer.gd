extends Node

var time := 0.0

func _ready():
	set_process(false)

func _process(delta):
	time += delta
