extends Node2D

const ZOMBIE := preload("res://objects/Zombie.tscn")
const HUMAN := preload("res://objects/Human.tscn")
const MUSIC := "nightwolfcfm_zombie-chase.mp3"

export var zombie_count := 50
export var human_count := 20
export(String, FILE, "*.tscn,*.scn") var next_level := ""

onready var tilemap:TileMap = get_node("%Map")
onready var _tree := get_tree()
onready var _left := $HUD/PeopleLeft

func _ready():
	randomize()
	make_mobs(ZOMBIE, zombie_count)
	make_mobs(HUMAN, human_count)
	GameTimer.set_process(true)
	if not SfxManager.is_sound_playing(MUSIC):
		SfxManager.play_sound(MUSIC)

func _process(delta):
	var people_left = len(_tree.get_nodes_in_group("humans"))
	_left.text = "Humans left: " + str(people_left)
	if people_left <= 0:
		Transition.change_scene(next_level)

func make_mobs(mob_scene:PackedScene, count:int):
	for i in range(count):
		var newmob = mob_scene.instance()
		var newpos = Vector2(rand_range(100, 924), rand_range(100, 500))
		
		newmob.position = newpos
		add_child(newmob)
