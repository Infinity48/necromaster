extends Control

func _ready():
	var time = GameTimer.time
	$Time.text = "You beat the game in %s:%02d"%[int(time) / 60, int(time) % 60]
